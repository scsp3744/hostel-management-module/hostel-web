import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

import { BlockService } from '../shared/block.service';
import { Block } from '../shared/block.model';

declare var M: any;

@Component({
  selector: 'app-block',
  templateUrl: './block.component.html',
  styleUrls: ['./block.component.css'],
  providers:[BlockService]
})
export class BlockComponent implements OnInit {

  constructor(public blockService: BlockService) { }

  ngOnInit(){
    this.resetForm();
    this.refreshBlockList();
  }

  resetForm(form?: NgForm){
    if(form)
      form.reset();
    this.blockService.selectedBlock = {
      _id:"",
      name:"",
      college:""
    }
  }

  onSubmit(form: NgForm){
    if(form.value._id == ""){
      this.blockService.postBlock(form.value).subscribe((res)=>{
        this.resetForm(form);
        this.refreshBlockList();
        M.toast({html:'Saved successfully',classes:'rounded'});
      });
    }
    else{
      this.blockService.putBlock(form.value).subscribe((res)=>{
        this.resetForm(form);
        this.refreshBlockList();
        M.toast({html:'Updated successfully',classes:'rounded'});
      });
    }
  }

  refreshBlockList(){
    this.blockService.getBlockList().subscribe((res)=>{
      this.blockService.blocks = res as Block[];
    });
  }

  onEdit(blok : Block){
    this.blockService.selectedBlock = blok;
  }

  onDelete(_id: string, form: NgForm){
    if(confirm('Are you sure to delete this record?')== true){
      this.blockService.deleteBlock(_id).subscribe((res)=>{
        this.refreshBlockList();
        this.resetForm(form);
        M.toast({html:'Deleted successfully',classes:'rounded'});
      });
    }
  }
}

