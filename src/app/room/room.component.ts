import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

import { RoomService } from '../shared/room.service';
import { Room } from '../shared/room.model';

declare var M: any;

@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.css'],
  providers: [RoomService]
})
export class RoomComponent implements OnInit {

  constructor(public roomService: RoomService) { }

  ngOnInit(){
    this.resetForm();
    this.refreshRoomList();
  }

  resetForm(form?: NgForm){
    if(form)
      form.reset();
    this.roomService.selectedRoom = {
      _id:"",
      roomno:0,
      roomtype:"",
      rentalrate:0,
      block:"",
      student:""
    }
  }

  onSubmit(form: NgForm){
    if(form.value._id == ""){
      this.roomService.postRoom(form.value).subscribe((res)=>{
        this.resetForm(form);
        this.refreshRoomList();
        M.toast({html:'Saved successfully',classes:'rounded'});
      });
    }
    else{
      this.roomService.putRoom(form.value).subscribe((res)=>{
        this.resetForm(form);
        this.refreshRoomList();
        M.toast({html:'Updated successfully',classes:'rounded'});
      });
    }
  }

  refreshRoomList(){
    this.roomService.getRoomList().subscribe((res)=>{
      this.roomService.rooms = res as Room[];
    });
  }

  onEdit(bilik : Room){
    this.roomService.selectedRoom = bilik;
  }

  onDelete(_id: string, form: NgForm){
    if(confirm('Are you sure to delete this record?')== true){
      this.roomService.deleteRoom(_id).subscribe((res)=>{
        this.refreshRoomList();
        this.resetForm(form);
        M.toast({html:'Deleted successfully',classes:'rounded'});
      });
    }
  }
}
