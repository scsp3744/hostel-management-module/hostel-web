import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { RoomComponent } from './room/room.component';
import { BlockComponent } from './block/block.component';
import { CollegeComponent } from './college/college.component';
import { StudentsComponent } from './students/students.component';

const routes: Routes = [
  {path:'rooms', component:RoomComponent},
  {path:'blocks', component:BlockComponent},
  {path:'colleges', component:CollegeComponent},
  {path:'students', component:StudentsComponent}
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
