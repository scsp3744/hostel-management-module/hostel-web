import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

import { CollegeService } from '../shared/college.service';
import { College } from '../shared/college.model';

declare var M: any;
@Component({
  selector: 'app-college',
  templateUrl: './college.component.html',
  styleUrls: ['./college.component.css'],
  providers: [CollegeService]
})
export class CollegeComponent implements OnInit {

  constructor(public collegeService: CollegeService) { }

  ngOnInit(){
    this.resetForm();
    this.refreshCollegeList();
  }

  resetForm(form?: NgForm){
    if(form)
      form.reset();
    this.collegeService.selectedCollege = {
      _id:"",
      name:"",
      address:""
    }
  }

  onSubmit(form: NgForm){
    if(form.value._id == ""){
      this.collegeService.postCollege(form.value).subscribe((res)=>{
        this.resetForm(form);
        this.refreshCollegeList();
        M.toast({html:'Saved successfully',classes:'rounded'});
      });
    }
    else{
      this.collegeService.putCollege(form.value).subscribe((res)=>{
        this.resetForm(form);
        this.refreshCollegeList();
        M.toast({html:'Updated successfully',classes:'rounded'});
      });
    }
  }

  refreshCollegeList(){
    this.collegeService.getCollegeList().subscribe((res)=>{
      this.collegeService.colleges = res as College[];
    });
  }

  onEdit(kolej : College){
    this.collegeService.selectedCollege = kolej;
  }

  onDelete(_id: string, form: NgForm){
    if(confirm('Are you sure to delete this record?')== true){
      this.collegeService.deleteRoom(_id).subscribe((res)=>{
        this.refreshCollegeList();
        this.resetForm(form);
        M.toast({html:'Deleted successfully',classes:'rounded'});
      });
    }
  }

}
