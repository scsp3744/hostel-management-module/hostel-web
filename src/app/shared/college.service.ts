import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
//import { toPromise } from 'rxjs/operators';

import { College } from './college.model';

@Injectable()
export class CollegeService {
  selectedCollege: College;
  colleges: College[];
  readonly baseURL = 'http://localhost:3000/college';

  constructor(public http: HttpClient) { }

  postCollege(kolej: College){
    return this.http.post(this.baseURL, kolej);
  }

  getCollegeList(){
    return this.http.get(this.baseURL);
  }

  putCollege(kolej: College){
    return this.http.put(this.baseURL + `/${kolej._id}`, kolej);
  }

  deleteRoom(_id: string){
    return this.http.delete(this.baseURL + `/${_id}` );
  }
}
