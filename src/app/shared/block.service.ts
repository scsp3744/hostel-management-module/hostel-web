import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
//import { toPromise } from 'rxjs/operators';

import { Block } from './block.model';

@Injectable()
export class BlockService {
  selectedBlock: Block;
  blocks: Block[];
  readonly baseURL = 'http://localhost:3000/block';

  constructor(public http: HttpClient) { }

  postBlock(blok: Block){
    return this.http.post(this.baseURL, blok);
  }

  getBlockList(){
    return this.http.get(this.baseURL);
  }

  putBlock(blok: Block){
    return this.http.put(this.baseURL + `/${blok._id}`, blok);
  }

  deleteBlock(_id: string){
    return this.http.delete(this.baseURL + `/${_id}` );
  }
}
