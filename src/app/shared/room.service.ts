import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
//import { toPromise } from 'rxjs/operators';

import { Room } from './room.model';

@Injectable()
export class RoomService {
  selectedRoom: Room;
  rooms: Room[];
  readonly baseURL = 'http://localhost:3000/room';

  constructor(public http: HttpClient) { }

  postRoom(bilik: Room){
    return this.http.post(this.baseURL, bilik);
  }

  getRoomList(){
    return this.http.get(this.baseURL);
  }

  putRoom(bilik: Room){
    return this.http.put(this.baseURL + `/${bilik._id}`, bilik);
  }

  deleteRoom(_id: string){
    return this.http.delete(this.baseURL + `/${_id}` );
  }
}
